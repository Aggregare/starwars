import { StarShipsComponent } from './components/star-ships/star-ships.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeopleComponent } from './components/people/people.component';

const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'home',
    component: HomeComponent
  },
  { path: '', 
     redirectTo: 'home', pathMatch: 'full'
  },
  {
    path:'starships/:name',
    component: StarShipsComponent
  },
  {
    path:'people/:name',
    component: PeopleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
