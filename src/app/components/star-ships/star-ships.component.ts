import { Observable, Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { StarWarsApiServiceService } from 'src/app/services/star-wars-api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-star-ships',
  templateUrl: './star-ships.component.html',
  styleUrls: ['./star-ships.component.scss']
})
export class StarShipsComponent implements OnInit {

  starShipsList: any;
  spaceShipDetails: any;
  peopleList: any;
  spaceShipName: string;
  paramSubscription: Subscription;

  constructor( private starwarsApi: StarWarsApiServiceService,   private activatedRoute: ActivatedRoute, private router: Router, private location: Location) { }

   ngOnInit() {

     this.starShipsList = this.starwarsApi.getStarShips();
      this.peopleList = this.starwarsApi.getPeople();

      this.paramSubscription = this.activatedRoute.params.subscribe((params) => {
      this.spaceShipName = params['name'];
      if (this.spaceShipName) {
           this.spaceShipDetails = this.starShipsList.find(x=> x.name === this.spaceShipName);
           console.log('this.spaceShipDetails',this.spaceShipDetails)
        }
      });
  }

  getPilotName(pilot) {
    return this.starwarsApi.getPilotName(pilot);
  }

  onHome() {
    this.router.navigate(
      ['/home/']
     );
  }

  onReturn() {
    this.location.back();
  }
  
  onLoadPilotDetail(pilot) {
    this.router.navigate(
      ['/people/', this.getPilotName(pilot)]
     );
  }


  
}
