import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StarWarsApiServiceService } from 'src/app/services/star-wars-api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  starShipsList: any;
  peopleList: any;
  planetList:any;

  constructor(private starwarsApi: StarWarsApiServiceService, private router: Router) { }

  ngOnInit(): void {
    this.starShipsList = this.starwarsApi.getStarShips();
    //console.log('this.starShipsList///',this.starShipsList)

    this.peopleList = this.starwarsApi.getPeople();
    //console.log('this.peopleList///',this.peopleList)

    this.planetList = this.starwarsApi.getAllPlanets().subscribe((planets) => {
      this.planetList = planets;
      console.log('this.planetList///',this.planetList)
    });

    // this.planetList = this.starwarsApi.getPlanets();
    // console.log('this.planetList',this.planetList)
    
  }

  
  getPilotName(pilot) {
    return this.starwarsApi.getPilotName(pilot);
  }

  onLoadPilotDetail(pilot) {
    this.router.navigate(
      ['/people/', this.getPilotName(pilot)]
     );
  }

  onLoadStarShipDetail(spaceship) {
    this.router.navigate(
      ['/starships/', spaceship.name]
     );
  }

}
