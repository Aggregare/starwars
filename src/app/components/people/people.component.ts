import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StarWarsApiServiceService } from 'src/app/services/star-wars-api-service.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  starShipsList: any;
  peopleDetails: any;
  peopleList: any;
  peopleName: string;
  paramSubscription: Subscription;

  constructor( private starwarsApi: StarWarsApiServiceService,   private activatedRoute: ActivatedRoute, private router: Router, private location: Location) { }

  ngOnInit() {

    this.starShipsList = this.starwarsApi.getStarShips();
      this.peopleList = this.starwarsApi.getPeople();

      this.paramSubscription = this.activatedRoute.params.subscribe((params) => {
      this.peopleName = params['name'];
      if (this.peopleName) {
          this.peopleDetails = this.peopleList.find(x=> x.name === this.peopleName);
          console.log('this.peopleDetails',this.peopleDetails)
        }
      });
  }

  getPilotName(pilot) {
    return this.starwarsApi.getPilotName(pilot);
  }

  onHome() {
    this.router.navigate(
      ['/home/']
    );
  }

  onReturn() {
    this.location.back();
  }
}