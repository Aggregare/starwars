import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { expand, mapTo, catchError, take, map, reduce } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { Planets } from '../models/planet.model';

 
const httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET',
      'Access-Control-Allow-Origin': '*',
     })
};
const BASE_URL='https://swapi.dev/api'


@Injectable({
  providedIn: 'root'
})
export class StarWarsApiServiceService {

  private _starShips$: Observable<any>;
  private _starShipsList = [];

  private _people$: Observable<any>;
  private _peopleList = [];

  private _planets$: Observable<any>
  private _planetsList= [];

  //private request ='';
  constructor(private http:HttpClient) { 

    console.log("fetching StarShips...")
    let request = BASE_URL + '/starships/?page=';

   this.http.get( request+'1').toPromise()
    .then ( (starShips) =>
      { 
          this._starShipsList.push(...starShips['results']);
          let numPages = Math.ceil((starShips['count']/10));
          // console.log("numPages ===",numPages);
          for (let page=2; page <= numPages; page++) 
          {
            this.http.get( request + page).toPromise()
            .then ( (starShips) => {
                this._starShipsList.push(...starShips['results']);
              }),
              (err) =>{
                console.log('Error paging===',err);
              }
          };
      }),
      (err) =>{
        console.log('Error fetching starships===',err);
      }


      console.log("fetching People/Pilots...")
      let peopleRequest = BASE_URL + '/people/?page=';

   this.http.get( peopleRequest+'1').toPromise()
    .then ( (people) =>
      { 
          this._peopleList.push(...people['results']);
          let numPages = Math.ceil((people['count']/10));
           console.log("numPages ===",numPages);
          for (let page=2; page <= numPages; page++) 
          {
            this.http.get( peopleRequest + page).toPromise()
            .then ( (people) => {
                this._peopleList.push(...people['results']);
                console.log("page ===",page);
              }),
              (err) =>{
                console.log('Error paging===',err);
              }
          };
          
      }),
      (err) =>{
        console.log('Error fetching people===',err);
      }


      // console.log("fetchin Planets...")
      // this.getAllPlanets().subscribe((planets) => {
      //   console.log('this._planetsList ===', this._planetsList)
      //   this._planetsList = planets
      // });
  }

  public getAllPlanets(): Observable<Planets[]> {

    return new Observable(observer => {
          this.getPage("http://swapi.dev/api/planets/").pipe(
            expand((data, i) => {
                  return data.next ? this.getPage(data.next) : EMPTY;
              }),
            reduce((acc, data) => {
                  return acc.concat(data.results);
                }, []),
            //catchError(error => of({error}))
          )
          //.catchError(error => observer.error(error))
          .subscribe((planet) => {
                observer.next(planet);
                observer.complete();
          });
    });
}

private getPage(url: string): Observable<{next: string, results: Planets[]}> {
  return this.http.get(url).pipe(
            map(response => {
                    let body = response;
                    return {
                      next: body['next'],
                      results: body['results'] as Planets[]
                    };
                }
            ));
}
  

    getStarShips() {
     return this._starShipsList; 
    }

    getPeople() {
      return this._peopleList; 
     }


  getPlanets() {
    return this._planetsList;
    }

    getPilotName(pilot) {
 
      let rst =  this._peopleList.find(x=> x['url'] == pilot);
      if (rst) return rst['name'];
      return '';
    }

}
